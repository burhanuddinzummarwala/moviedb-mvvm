package com.themoviedb.mvvm.movies_list;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.themoviedb.mvvm.R;
import com.themoviedb.mvvm.databinding.ItemMovieBinding;
import com.themoviedb.mvvm.infra.api.model.Movie;

import static com.themoviedb.mvvm.infra.api.Endpoints.IMAGE_URL_PREFIX;

public class MovieAdapter extends PagedListAdapter<Movie, MovieAdapter.MovieViewHolder> {

    private ListItemClickListener listItemClickListener;
    private Context mContext;

    public MovieAdapter(ListItemClickListener listItemClickListener) {
        super(new MovieItemCallback());
        this.listItemClickListener = listItemClickListener;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        mContext = viewGroup.getContext();
        return MovieViewHolder.create(viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder viewHolder, int position) {
        Movie item = getItem(position);
        if (item != null) {
            viewHolder.bind(item);
            Glide.with(mContext.getApplicationContext())
                    .load(IMAGE_URL_PREFIX + item.poster_path)
                    .apply(new RequestOptions().placeholder(R.drawable.splash_logo).error(R.drawable.splash_logo))
                    .into(viewHolder.binding.imThumb);
            viewHolder.itemView.setOnClickListener(v -> listItemClickListener.onClickItem(v, item));
        }
    }

    public static class MovieViewHolder extends RecyclerView.ViewHolder {
        ItemMovieBinding binding;

        public MovieViewHolder(ItemMovieBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        static MovieViewHolder create(ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            ItemMovieBinding binding = ItemMovieBinding.inflate(layoutInflater, parent, false);
            return new MovieViewHolder(binding);
        }

        public void bind(Movie item) {
            binding.setItem(item);
            binding.executePendingBindings();
        }
    }
}
