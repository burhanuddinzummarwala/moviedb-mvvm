package com.themoviedb.mvvm.movies_list;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.themoviedb.mvvm.infra.api.ApiService;
import com.themoviedb.mvvm.infra.api.model.Movie;
import com.themoviedb.mvvm.infra.livedata.SingleLiveEvent;
import com.themoviedb.mvvm.infra.livedata.SnackbarMessage;
import com.themoviedb.mvvm.infra.livedata.SnackbarMessageString;

import javax.inject.Inject;

public class MoviesListViewModel extends AndroidViewModel {

    public static SingleLiveEvent<String> navigation = new SingleLiveEvent<>();
    public static SingleLiveEvent<String> apiStatus = new SingleLiveEvent<>();
    public static final SnackbarMessage snackbarMessage = new SnackbarMessage();
    public static SnackbarMessageString snackbarMessageString = new SnackbarMessageString();

    public final DataModel dataModel = new DataModel();
    public final EventHandler eventHandler = new EventHandler(this);

    public LiveData<PagedList<Movie>> movieList;
    private MovieDataSourceFactory movieDataSourceFactory;

    @Inject
    public MoviesListViewModel(Application application,
                               MovieDataSourceFactory movieDataSourceFactory)
    {
        super(application);
        this.movieDataSourceFactory = movieDataSourceFactory;
    }

    public void setUpPagedList(){
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(ApiService.LOAD_PAGE_SIZE)
                .setPageSize(ApiService.LOAD_PAGE_SIZE)
                .build();
        movieList = new LivePagedListBuilder<>(movieDataSourceFactory, config)
                .build();
    }


    /**
     * DataModel
     */
    public static class DataModel {
        //no-op
    }

    /**
     * EventHandler
     */
    public static class EventHandler {
        private final MoviesListViewModel viewModel;

        public EventHandler(MoviesListViewModel viewModel) {
            this.viewModel = viewModel;
        }
    }
}
