package com.themoviedb.mvvm.movies_list;

import android.view.View;

import com.themoviedb.mvvm.infra.api.model.Movie;

public interface ListItemClickListener {
    void onClickItem(View view, Movie movieItem);
}