package com.themoviedb.mvvm.movies_list;

import android.app.Application;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;
import android.util.Log;

import com.themoviedb.mvvm.R;
import com.themoviedb.mvvm.infra.api.ApiService;
import com.themoviedb.mvvm.infra.api.model.Movie;
import com.themoviedb.mvvm.infra.api.model.MovieListResponse;
import com.themoviedb.mvvm.infra.db.MovieDao;
import com.themoviedb.mvvm.infra.utils.ConnectionDetector;
import com.themoviedb.mvvm.infra.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Response;

import static com.themoviedb.mvvm.infra.utils.Constants.IS_NOW_PLAYING;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_POPULAR;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_TOP_RATED;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_UPCOMING;
import static com.themoviedb.mvvm.infra.viewmodel.ApiStatus.HIDE_PROGRESS;

public class MovieDataSource extends PageKeyedDataSource<Integer, Movie> {
    private static final String TAG = "MovieDataSource";

    private ApiService apiService;
    private Application application;
    private MovieDao movieDao;

    public MovieDataSource(ApiService apiService, Application application) {
        this.apiService = apiService;
        this.application = application;
    }

    public void setData(MovieDao movieDao) {
        Log.e(TAG, "setData: movieDao");
        this.movieDao = movieDao;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Movie> callback) {
        if (application != null && apiService != null) {
            try {
                if (ConnectionDetector.isConnectingToInternet(application)) {
                    Log.e(TAG, "loadInitial: ");
                    Response<MovieListResponse> response = null;
                    if(IS_POPULAR)
                        response = apiService.getPopularMovies(application.getString(R.string.api_key), "en-US", 1).execute();
                    else if(IS_TOP_RATED)
                        response = apiService.getTopRatedMovies(application.getString(R.string.api_key), "en-US", 1).execute();
                    else if(IS_UPCOMING)
                        response = apiService.getUpcomingMovies(application.getString(R.string.api_key), "en-US", 1).execute();
                    else if(IS_NOW_PLAYING)
                        response = apiService.getNowPlayingMovies(application.getString(R.string.api_key), "en-US", 1).execute();

                    if (response != null && response.body() != null && response.isSuccessful()) {
                        if (response.body().results.size() > 0) {

                            ArrayList<Movie> tempList = new ArrayList<>();
                            for (int i = 0; i < response.body().results.size(); i++) {
                                Movie movie = response.body().results.get(i);
                                tempList.add(movie);
                            }
                            movieDao.cleanInsertAll(tempList);
                        } else {
                            movieDao.deleteAll();
                        }
                        callback.onResult(response.body().results, 1, 2);
                        MoviesListViewModel.apiStatus.postValue(HIDE_PROGRESS);
                    } else {
                        try {
                            String errorBody = response.errorBody().string();
                            JSONObject jsonObject = new JSONObject(errorBody);
                            JSONArray jsonArray = jsonObject.getJSONArray("errors");
                            if (jsonArray.length() > 0) {
                                MoviesListViewModel.snackbarMessageString.postValue(jsonArray.get(0).toString());
                            }
                        } catch (IOException | JSONException e1) {
                            e1.printStackTrace();
                        }

                        MoviesListViewModel.apiStatus.postValue(HIDE_PROGRESS);
                        callback.onResult(Collections.emptyList(), 1, 1);
                    }

                } else {
                    callback.onResult(movieDao.getAll(), 1, 2);
                    MoviesListViewModel.snackbarMessage.postValue(R.string.msg_alert_no_internet);
                }
            } catch (IOException e) {
                Log.e(TAG, "loadInitial: " + e.getLocalizedMessage());
                MoviesListViewModel.snackbarMessageString.postValue(e.getLocalizedMessage());
                callback.onResult(Collections.emptyList(), 1, 1);
                MoviesListViewModel.apiStatus.postValue(HIDE_PROGRESS);
            } catch (Exception e){
                Log.e(TAG, "loadInitial: " + e.getLocalizedMessage());
                MoviesListViewModel.snackbarMessageString.postValue(e.getLocalizedMessage());
                callback.onResult(Collections.emptyList(), 1, 1);
                MoviesListViewModel.apiStatus.postValue(HIDE_PROGRESS);
            }
        }
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Movie> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Movie> callback) {
        if (application != null && ConnectionDetector.isConnectingToInternet(application)) {
            try {
                Response<MovieListResponse> response = null;

                if(IS_POPULAR)
                    response = apiService.getPopularMovies(application.getString(R.string.api_key), "en-US", params.key).execute();
                else if(IS_TOP_RATED)
                    response = apiService.getTopRatedMovies(application.getString(R.string.api_key), "en-US", params.key).execute();
                else if(IS_UPCOMING)
                    response = apiService.getUpcomingMovies(application.getString(R.string.api_key), "en-US", params.key).execute();
                else if(IS_NOW_PLAYING)
                    response = apiService.getNowPlayingMovies(application.getString(R.string.api_key), "en-US", params.key).execute();

                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().results.size() > 0) {
                        ArrayList<Movie> tempList = new ArrayList<>();
                        for (int i = 0; i < response.body().results.size(); i++) {
                            Movie movie = response.body().results.get(i);
                            tempList.add(movie);
                        }

                        movieDao.insertAll(tempList);
                    }
                    callback.onResult(response.body().results, params.key + 1);
                } else {
                    callback.onResult(Collections.emptyList(), params.key);
                }
            } catch (IOException e) {
                Log.e(TAG, "loadAfter page: " + params.key, e);
                MoviesListViewModel.snackbarMessageString.postValue(e.getLocalizedMessage());
                callback.onResult(Collections.emptyList(), params.key);
            } catch (Exception e) {
                Log.e(TAG, "loadInitial", e);
                MoviesListViewModel.snackbarMessageString.postValue(e.getLocalizedMessage());
                callback.onResult(Collections.emptyList(), params.key);
            }
        }
    }
}
