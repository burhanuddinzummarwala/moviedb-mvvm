package com.themoviedb.mvvm.movies_list;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.navigation.Navigation;

import com.google.gson.Gson;
import com.themoviedb.mvvm.MovieDBApplication;
import com.themoviedb.mvvm.R;
import com.themoviedb.mvvm.databinding.FragmentMoviesListBinding;
import com.themoviedb.mvvm.infra.api.model.Movie;
import com.themoviedb.mvvm.infra.utils.ConnectionDetector;
import com.themoviedb.mvvm.infra.utils.KeyboardUtil;
import com.themoviedb.mvvm.infra.utils.Utils;
import com.themoviedb.mvvm.infra.viewmodel.AppViewModelFactory;

import java.util.Objects;

import javax.inject.Inject;

import static com.themoviedb.mvvm.infra.utils.Constants.IS_NOW_PLAYING;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_POPULAR;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_TOP_RATED;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_UPCOMING;
import static com.themoviedb.mvvm.infra.viewmodel.ApiStatus.HIDE_PROGRESS;
import static com.themoviedb.mvvm.infra.viewmodel.ApiStatus.SHOW_PROGRESS;

public class MoviesListFragment extends Fragment implements ListItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "MoviesListFragment";

    @Inject
    AppViewModelFactory viewModelFactory;

    MoviesListViewModel viewModel;

    private FragmentMoviesListBinding binding;

    private MovieAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MovieDBApplication.getInstance().getAppComponent().inject(this);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MoviesListViewModel.class);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movies_list, container, false);
        binding.setViewModel(viewModel);

        setUpMvvm();
        setUpMoviesListView();

        return binding.getRoot();
    }

    private void setUpMvvm() {
        MoviesListViewModel.navigation.observe(getViewLifecycleOwner(), navigation -> {
            if (navigation == null)
                return;
        });

        MoviesListViewModel.apiStatus.observe(getViewLifecycleOwner(), status -> {
            Log.e(TAG, "status: "+status);
            KeyboardUtil.hideKeyboard(Objects.requireNonNull(getActivity()));
            binding.swipeContainer.setRefreshing(false);

            if (SHOW_PROGRESS.equals(status) && getActivity() != null) {
                Utils.showProgress(getActivity());
            } else if (HIDE_PROGRESS.equals(status)) {
                Utils.stopProgress();
            }
        });

        MoviesListViewModel.snackbarMessageString.observe(getViewLifecycleOwner(), (Observer<String>) stringResId -> {
            KeyboardUtil.hideKeyboard(Objects.requireNonNull(getActivity()));
            Snackbar.make(getView(), stringResId, Snackbar.LENGTH_LONG).show();
        });

        MoviesListViewModel.snackbarMessage.observe(getViewLifecycleOwner(), (Observer<Integer>) stringResId -> {
            KeyboardUtil.hideKeyboard(getActivity());
            Snackbar.make(getView(), stringResId, Snackbar.LENGTH_LONG).show();
        });
    }

    public void setUpMoviesListView() {
        setUpActionBarTitle();

        MoviesListViewModel.apiStatus.setValue(SHOW_PROGRESS);
        viewModel.setUpPagedList();

        Context context = getContext();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        binding.moviesList.setLayoutManager(layoutManager);

        adapter = new MovieAdapter(this);
        binding.moviesList.setAdapter(adapter);

        viewModel.movieList.observe(this, pagedList -> {
            if (pagedList == null || pagedList.size() == 0) {
                binding.fmlTxtNoData.setVisibility(View.VISIBLE);
                binding.moviesList.setVisibility(View.GONE);
            } else {
                binding.fmlTxtNoData.setVisibility(View.GONE);
                binding.moviesList.setVisibility(View.VISIBLE);
                adapter.submitList(pagedList);
            }

            MoviesListViewModel.apiStatus.setValue(HIDE_PROGRESS);
        });

        binding.swipeContainer.setOnRefreshListener(this);
    }

    private void setUpActionBarTitle() {
        if (getActivity() != null && isAdded()) {
            if (IS_UPCOMING)
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.upcoming));
            else if (IS_TOP_RATED)
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.top_rated));
            else if (IS_NOW_PLAYING)
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.now_playing));
            else if (IS_POPULAR)
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.popular));
        }
    }

    @Override
    public void onClickItem(View view, Movie movieItem) {
        Bundle args = new Bundle();
        args.putString("ARG_1", new Gson().toJson(movieItem));
        Navigation.findNavController(getView()).navigate(R.id.action_fragment_movies_list_to_fragment_movie_detail, args);
    }

    @Override
    public void onRefresh() {
        binding.swipeContainer.setRefreshing(false);
        setUpMoviesListView();
    }
}
