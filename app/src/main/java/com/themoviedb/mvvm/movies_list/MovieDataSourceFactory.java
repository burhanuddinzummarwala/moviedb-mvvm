package com.themoviedb.mvvm.movies_list;

import android.app.Application;
import android.arch.paging.DataSource;

import com.themoviedb.mvvm.infra.api.ApiService;
import com.themoviedb.mvvm.infra.api.model.Movie;
import com.themoviedb.mvvm.infra.db.AppDatabase;
import com.themoviedb.mvvm.infra.db.MovieDao;

import javax.inject.Inject;

public class MovieDataSourceFactory extends DataSource.Factory<Integer, Movie> {

    private MovieDao movieDao;
    private ApiService apiService;
    private Application application;

    @Inject
    public MovieDataSourceFactory(ApiService apiService, AppDatabase appDatabase, Application application) {
        this.apiService = apiService;
        this.movieDao = appDatabase.movieDao();
        this.application = application;
    }

    @Override
    public DataSource<Integer, Movie> create() {
        MovieDataSource movieDataSource = new MovieDataSource(apiService, application);
        movieDataSource.setData(movieDao);
        return movieDataSource;
    }
}
