package com.themoviedb.mvvm.movies_list;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;

import com.themoviedb.mvvm.infra.api.model.Movie;

public class MovieItemCallback extends DiffUtil.ItemCallback<Movie> {

    @Override
    public boolean areItemsTheSame(@NonNull Movie movie1, @NonNull Movie movie2) {
        return movie1.id == movie2.id;
    }

    @Override
    public boolean areContentsTheSame(@NonNull Movie movie1, @NonNull Movie movie2) {
        return movie1.equals(movie2);
    }
}
