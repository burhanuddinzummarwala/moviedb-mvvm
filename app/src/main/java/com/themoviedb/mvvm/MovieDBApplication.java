package com.themoviedb.mvvm;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.themoviedb.mvvm.infra.di.AppComponent;
import com.themoviedb.mvvm.infra.di.AppModule;
import com.themoviedb.mvvm.infra.di.DaggerAppComponent;
import com.themoviedb.mvvm.infra.di.NetworkModule;

public class MovieDBApplication extends MultiDexApplication {
    private static final String TAG = "MovieDBApplication";
    private static MovieDBApplication instance;
    private AppComponent appComponent;

    public static MovieDBApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        setupDependencyInjection();
        MovieDBApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void setupDependencyInjection() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
