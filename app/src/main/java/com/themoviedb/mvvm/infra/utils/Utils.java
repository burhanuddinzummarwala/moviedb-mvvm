package com.themoviedb.mvvm.infra.utils;

import android.app.Dialog;
import android.content.Context;

import com.themoviedb.mvvm.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Utils {
    public static String convertTimeToTimeZone(String sdfFormat, String dateTime) {
        SimpleDateFormat sourceFormat = new SimpleDateFormat(sdfFormat);
        //sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date parsed = sourceFormat.parse(dateTime);
            //sourceFormat.setTimeZone(TimeZone.getDefault());
            String timeData = sourceFormat.format(parsed);
            return timeData;
        } catch (ParseException e) {
            e.printStackTrace();
            return dateTime;
        }
    }

    private static Dialog progressDialog;

    public static void showProgress(Context context) {
        stopProgress();
        progressDialog = new Dialog(context);
        progressDialog.setContentView(R.layout.custom_dialog);
        progressDialog.setCancelable(false);
        //progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Objects.requireNonNull(progressDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.show();
    }

    public static void stopProgress() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
