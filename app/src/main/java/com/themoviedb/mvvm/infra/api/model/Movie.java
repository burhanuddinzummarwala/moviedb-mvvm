package com.themoviedb.mvvm.infra.api.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Movie {
    @PrimaryKey
    @NonNull
    public String id;

    public double vote_count = 0;
    public String vote_average = "0";
    public String title = "";
    public double popularity = 0;
    public String original_language = "";
    public String original_title = "";
    public String overview = "";
    public String release_date = "";
    public String poster_path = "";
    public String backdrop_path = "";
}
