package com.themoviedb.mvvm.infra.viewmodel;

import android.arch.lifecycle.ViewModel;

import com.themoviedb.mvvm.home.HomeViewModel;
import com.themoviedb.mvvm.movie_detail.MovieDetailViewModel;
import com.themoviedb.mvvm.movies_list.MoviesListViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel.class)
    abstract ViewModel bindHomeViewModel(HomeViewModel homeViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MoviesListViewModel.class)
    abstract ViewModel bindMoviesListViewModel(MoviesListViewModel moviesListViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailViewModel.class)
    abstract ViewModel bindMoviewDetailViewModel(MovieDetailViewModel movieDetailViewModel);
}
