package com.themoviedb.mvvm.infra.viewmodel;

public class AppNavigation {
    
    private AppNavigation() {
        // no-op
    }

    public static final String NAVIGATE_BACK = "NAVIGATE_BACK";
}
