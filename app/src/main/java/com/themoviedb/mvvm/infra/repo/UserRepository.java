package com.themoviedb.mvvm.infra.repo;

import android.content.SharedPreferences;

import javax.inject.Inject;

public class UserRepository {
    static final String PREF_SELECTED_MOVIE_FILTER = "PREF_SELECTED_MOVIE_FILTER";

    private SharedPreferences sharedPreferences;

    @Inject
    public UserRepository(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public int getSelectedMovieFilterIndex(){
        return sharedPreferences.getInt(PREF_SELECTED_MOVIE_FILTER, 0);
    }

    public void setSelectedMovieFilterIndex(int index){
        sharedPreferences.edit().putInt(PREF_SELECTED_MOVIE_FILTER, index).apply();
    }
}
