package com.themoviedb.mvvm.infra.di;

import com.themoviedb.mvvm.MovieDBApplication;
import com.themoviedb.mvvm.home.HomeActivity;
import com.themoviedb.mvvm.infra.viewmodel.ViewModelModule;
import com.themoviedb.mvvm.movie_detail.MovieDetailFragment;
import com.themoviedb.mvvm.movies_list.MoviesListFragment;
import com.themoviedb.mvvm.splash.SplashActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, ViewModelModule.class})
public interface AppComponent {

    void inject(MovieDBApplication application);

    void inject(SplashActivity splashActivity);

    void inject(HomeActivity homeActivity);

    void inject(MoviesListFragment moviesListFragment);

    void inject(MovieDetailFragment movieDetailFragment);
}
