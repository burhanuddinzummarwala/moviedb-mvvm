package com.themoviedb.mvvm.infra.api;

import com.themoviedb.mvvm.BuildConfig;

public class Endpoints {
    public static String ENDPOINT_DEV = "https://api.themoviedb.org/3/movie/";
    public static String ENDPOINT_PROD = "https://api.themoviedb.org/3/movie/";

    public static String IMAGE_URL_PREFIX = "https://image.tmdb.org/t/p/w185_and_h278_bestv2";

    public static String getEndpoint() {

        if (BuildConfig.DEBUG)
            return ENDPOINT_DEV;
        else
            return ENDPOINT_PROD;
    }
}
