package com.themoviedb.mvvm.infra.databinding;

import android.databinding.BindingAdapter;
import android.support.design.widget.TextInputLayout;
import android.widget.TextView;

import com.themoviedb.mvvm.infra.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AppBindings {

    @BindingAdapter("errorText")
    public static void setErrorMessage(TextInputLayout view, String errorMessage) {
        view.setError(errorMessage);
    }


    @BindingAdapter("setDate")
    public static void setDate(TextView txtDate, String date) {
        if (!date.equalsIgnoreCase("null")) {
            //2018-08-30T21:34:40.000Z
            String currentStringFormat = "yyyy-MM-dd";
            String desireTimeFormat = "MMM dd, yyyy";
            SimpleDateFormat format = new SimpleDateFormat(currentStringFormat, Locale.getDefault());
            try {
                Date newDate = format.parse(date);
                SimpleDateFormat desireFormat = new SimpleDateFormat(desireTimeFormat, Locale.getDefault());
                txtDate.setText(Utils.convertTimeToTimeZone(desireTimeFormat, desireFormat.format(newDate)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //txtDate.setText(date);
    }

    @BindingAdapter("setRate")
    public static void setRate(TextView txtDate, String averageVote) {
        double result = Double.parseDouble(averageVote);
        if (result == 0)
            txtDate.setText("NR");
        else {
            txtDate.setText(((int) (result * 10)) + "%");
        }
    }
}
