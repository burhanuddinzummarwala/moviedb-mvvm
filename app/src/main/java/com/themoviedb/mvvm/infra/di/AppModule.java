package com.themoviedb.mvvm.infra.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.themoviedb.mvvm.MovieDBApplication;
import com.themoviedb.mvvm.infra.db.AppDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private MovieDBApplication application;

    public AppModule(MovieDBApplication application) {
        this.application = application;
    }

    @Provides
    public Context provideContext() {
        return application;
    }

    @Provides
    public Application provideApplication() {
        return application;
    }

    @Provides
    @Singleton
    public AppDatabase provideAppDatabase(Application application) {
        return AppDatabase.create(application);
    }

    @Provides
    public SharedPreferences provideDefaultSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
