package com.themoviedb.mvvm.infra.api.model;

import java.util.List;

public class MovieListResponse {
    public List<Movie> results;
}
