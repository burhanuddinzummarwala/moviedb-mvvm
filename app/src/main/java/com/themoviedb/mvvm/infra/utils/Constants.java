package com.themoviedb.mvvm.infra.utils;

import com.themoviedb.mvvm.infra.api.Endpoints;

public class Constants {
    public static String BASE_URL = Endpoints.getEndpoint();

    public static Boolean IS_POPULAR = false;
    public static Boolean IS_TOP_RATED = false;
    public static Boolean IS_UPCOMING = false;
    public static Boolean IS_NOW_PLAYING = false;
}
