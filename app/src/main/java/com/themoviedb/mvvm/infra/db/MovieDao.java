package com.themoviedb.mvvm.infra.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.themoviedb.mvvm.infra.api.model.Movie;

import java.util.List;

@Dao
public abstract class MovieDao {

    @Query("SELECT * FROM movie")
    public abstract List<Movie> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertAll(List<Movie> loadList);

    @Query("DELETE FROM movie")
    public abstract void deleteAll();

    @Transaction
    public void cleanInsertAll(List<Movie> movies) {
        deleteAll();
        insertAll(movies);
    }
}
