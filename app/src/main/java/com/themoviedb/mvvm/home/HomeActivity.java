package com.themoviedb.mvvm.home;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.themoviedb.mvvm.MovieDBApplication;
import com.themoviedb.mvvm.R;
import com.themoviedb.mvvm.databinding.ActivityHomeBinding;
import com.themoviedb.mvvm.infra.repo.UserRepository;
import com.themoviedb.mvvm.infra.utils.ConnectionDetector;
import com.themoviedb.mvvm.infra.utils.Constants;
import com.themoviedb.mvvm.infra.utils.KeyboardUtil;
import com.themoviedb.mvvm.movie_detail.MovieDetailFragment;
import com.themoviedb.mvvm.movies_list.MoviesListFragment;
import com.themoviedb.mvvm.movies_list.MoviesListViewModel;

import javax.inject.Inject;

import static com.themoviedb.mvvm.infra.utils.Constants.IS_NOW_PLAYING;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_POPULAR;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_TOP_RATED;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_UPCOMING;

public class HomeActivity extends AppCompatActivity {
    private static final String TAG = "HomeActivity";
    @Inject
    HomeViewModel viewModel;
    private ActivityHomeBinding binding;

    private NavController navController;
    private NavHostFragment navHostFragment;

    private boolean isOptionMenuShow = true;

    @Inject
    UserRepository userRepository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        MovieDBApplication.getInstance().getAppComponent().inject(this);

        binding.setViewModel(viewModel);

        setupNavigation();
        setUpMvvm();
    }

    private void setUpMvvm() {

    }

    private void setupNavigation() {
        navController = Navigation.findNavController(this, R.id.navigation_host_fragment);
        navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_host_fragment);
        setSupportActionBar((Toolbar) binding.toolbar);
        NavigationUI.setupActionBarWithNavController(this, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation_filter, menu);
        MenuItem menuItem = menu.findItem(R.id.action_filter);
        menuItem.setVisible(isOptionMenuShow);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_popular:
                if (!IS_POPULAR) {
                    if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                        userRepository.setSelectedMovieFilterIndex(0);
                        IS_POPULAR = true;
                        IS_NOW_PLAYING = false;
                        IS_TOP_RATED = false;
                        IS_UPCOMING = false;

                        updateRecyclerView();
                    } else {
                        MoviesListViewModel.snackbarMessage.setValue(R.string.msg_alert_no_internet);
                    }

                }
                break;
            case R.id.action_top_rated:
                if (!IS_TOP_RATED) {
                    if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                        userRepository.setSelectedMovieFilterIndex(1);
                        IS_POPULAR = false;
                        IS_NOW_PLAYING = false;
                        IS_TOP_RATED = true;
                        IS_UPCOMING = false;

                        updateRecyclerView();
                    } else {
                        MoviesListViewModel.snackbarMessage.setValue(R.string.msg_alert_no_internet);
                    }
                }

                break;
            case R.id.action_upcoming:
                if (!IS_UPCOMING) {
                    if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                        userRepository.setSelectedMovieFilterIndex(2);
                        IS_POPULAR = false;
                        IS_NOW_PLAYING = false;
                        IS_TOP_RATED = false;
                        IS_UPCOMING = true;

                        updateRecyclerView();
                    } else {
                        MoviesListViewModel.snackbarMessage.setValue(R.string.msg_alert_no_internet);
                    }

                }

                break;
            case R.id.action_now_playing:
                if (!IS_NOW_PLAYING) {
                    if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                        userRepository.setSelectedMovieFilterIndex(3);
                        IS_POPULAR = false;
                        IS_NOW_PLAYING = true;
                        IS_TOP_RATED = false;
                        IS_UPCOMING = false;

                        updateRecyclerView();
                    } else {
                        MoviesListViewModel.snackbarMessage.setValue(R.string.msg_alert_no_internet);
                    }

                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        assert navHostFragment != null;
        if (navHostFragment.getChildFragmentManager().getFragments().get(0) instanceof MovieDetailFragment) {
            showHideOptionMenu(true);
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void showHideOptionMenu(boolean isOptionMenuShow) {
        this.isOptionMenuShow = isOptionMenuShow;
        invalidateOptionsMenu();
    }

    void updateRecyclerView() {
        ((MoviesListFragment) navHostFragment.getChildFragmentManager().getFragments().get(0)).setUpMoviesListView();
    }
}
