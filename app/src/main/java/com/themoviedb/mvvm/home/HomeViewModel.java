package com.themoviedb.mvvm.home;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

import com.themoviedb.mvvm.infra.livedata.SnackbarMessage;

import javax.inject.Inject;

public class HomeViewModel extends AndroidViewModel {
    private static final String TAG = "HomeViewModel";

    public final SnackbarMessage snackbarMessage = new SnackbarMessage();
    public final DataModel dataModel = new DataModel();
    public final EventHandler eventHandler = new EventHandler(this);

    @Inject
    public HomeViewModel(Application application) {
        super(application);
    }

    /**
     * DataModel
     */
    public static class DataModel {
        //no-op
    }

    /**
     * EventHandler
     */
    public static class EventHandler {
        private final HomeViewModel viewModel;

        public EventHandler(HomeViewModel viewModel) {
            this.viewModel = viewModel;
        }
    }
}
