package com.themoviedb.mvvm.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.themoviedb.mvvm.MovieDBApplication;
import com.themoviedb.mvvm.home.HomeActivity;
import com.themoviedb.mvvm.infra.repo.UserRepository;

import javax.inject.Inject;

import static com.themoviedb.mvvm.infra.utils.Constants.IS_NOW_PLAYING;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_POPULAR;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_TOP_RATED;
import static com.themoviedb.mvvm.infra.utils.Constants.IS_UPCOMING;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = "SplashActivity";
    @Inject
    UserRepository userRepository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MovieDBApplication.getInstance().getAppComponent().inject(this);

        new Handler().postDelayed(() -> {
            startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            finish();
        }, 2000);

        switch (userRepository.getSelectedMovieFilterIndex()) {
            case 0:
                IS_POPULAR = true;
                break;
            case 1:
                IS_TOP_RATED = true;
                break;
            case 2:
                IS_UPCOMING = true;
                break;
            case 3:
                IS_NOW_PLAYING = true;
                break;
        }
    }
}
