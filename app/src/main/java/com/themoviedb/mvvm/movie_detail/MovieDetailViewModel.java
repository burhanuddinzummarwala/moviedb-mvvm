package com.themoviedb.mvvm.movie_detail;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

import com.themoviedb.mvvm.infra.livedata.SingleLiveEvent;

import javax.inject.Inject;

public class MovieDetailViewModel extends AndroidViewModel {

    final SingleLiveEvent<String> navigation = new SingleLiveEvent<>();

    @Inject
    public MovieDetailViewModel(Application application) {
        super(application);
    }
}
