package com.themoviedb.mvvm.movie_detail;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.navigation.Navigation;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.themoviedb.mvvm.MovieDBApplication;
import com.themoviedb.mvvm.R;
import com.themoviedb.mvvm.databinding.FragmentMovieDetailBinding;
import com.themoviedb.mvvm.home.HomeActivity;
import com.themoviedb.mvvm.infra.api.model.Movie;
import com.themoviedb.mvvm.infra.viewmodel.AppNavigation;
import com.themoviedb.mvvm.infra.viewmodel.AppViewModelFactory;

import javax.inject.Inject;

import static com.themoviedb.mvvm.infra.api.Endpoints.IMAGE_URL_PREFIX;

public class MovieDetailFragment extends Fragment {

    @Inject
    AppViewModelFactory appViewModelFactory;

    MovieDetailViewModel viewModel;

    private FragmentMovieDetailBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MovieDBApplication.getInstance().getAppComponent().inject(this);
        viewModel = ViewModelProviders.of(this, appViewModelFactory).get(MovieDetailViewModel.class);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_detail, container, false);
        binding.setViewModel(viewModel);

        initData();
        setUpMvvm();

        return binding.getRoot();
    }

    private void setUpMvvm() {
        viewModel.navigation.observe(getViewLifecycleOwner(), navigation -> {
            if (AppNavigation.NAVIGATE_BACK.equals(navigation)) {
                Navigation.findNavController(getView()).popBackStack();
            }
        });
    }

    private void initData() {
        if (getActivity() != null && getActivity() instanceof HomeActivity)
            ((HomeActivity) getActivity()).showHideOptionMenu(false);

        if (getArguments() != null && getArguments().getString("ARG_1") != null) {
            Movie bundleResponse = new Gson().fromJson(getArguments().getString("ARG_1"), new TypeToken<Movie>() {
            }.getType());
            binding.setItem(bundleResponse);

            if(getActivity() != null && isAdded()){
                Glide.with(getActivity())
                        .load(IMAGE_URL_PREFIX + bundleResponse.poster_path)
                        .apply(new RequestOptions().placeholder(R.drawable.splash_logo).error(R.drawable.splash_logo))
                        .into(binding.imThumb);

                Glide.with(getActivity())
                        .load(IMAGE_URL_PREFIX + bundleResponse.backdrop_path)
                        .apply(new RequestOptions().placeholder(R.drawable.splash_logo).error(R.drawable.splash_logo))
                        .into(binding.fmdBackThumb);
            }
        }
    }
}
